Source: pyutilib
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>
Section: python
Testsuite: autopkgtest-pkg-python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-six,
               python3-xlrd <!nocheck>,
               python3-yaml <!nocheck>,
               python3-nose <!nocheck>,
               valgrind <!nocheck>
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/python-team/packages/pyutilib
Vcs-Git: https://salsa.debian.org/python-team/packages/pyutilib.git
Homepage: https://github.com/PyUtilib/pyutilib
Rules-Requires-Root: no

Package: python3-pyutilib
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends}
Recommends: python3-pyro4
Suggests: valgrind
Description: Python 3 library featuring development utilities
 Pyutilib is a ensemble of Python packages that includes a wide variety
 of utilities for software development.
 .
 The highlight features of this package are:
  - pyutilib.component: well-developed architecture for managing software
    components in complex Python applications
  - pyutilib.workflow: Python classes that provide an intuitive interface
    for defining and executing scientific workflows
  - pyutilib.autotest: automates the setup of test suites from test
    configuration files
  - pyutilib.th: utilities for testing Python software. The main component is an
    extension of Unittest to support new testing capabilities
 .
 Plus, the package includes a couple of useful helper scripts.
