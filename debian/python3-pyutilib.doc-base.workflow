Document: pyutilib-workflow
Title: Managing scientific workflows in Python with pyutilib.workflow
Author: William E. Hart
Abstract: A description of the pyutilib.workflow software package.
Section: Programming/Python

Format: PDF
Files: /usr/share/doc/python3-pyutilib/workflow.pdf.gz